package by.bsuir.aipos.sem6.restfulservice;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by Dzmitry Saladukha on 18.04.2016.
 */
@XmlRootElement(name = "book")
public class Book implements Serializable {

    private static final long serialVersionUID = 5866651575341222537L;
    private String name;
    private Author author;
    private String publisher;
    private Integer edition;
    private Integer tomesNumber;
    private Integer totalCount;

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String string) {
        this.name = string;
    }

    @XmlElement(type = Author.class)
    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @XmlElement(name = "publisher")
    public String getPubl() {
        return publisher;
    }

    public void setPubl(String string) {
        this.publisher = string;
    }

    @XmlElement(name = "volumeNumber")
    public Integer getNumb() {
        return tomesNumber;
    }

    public void setNumb(Integer integer) {
        this.tomesNumber = integer;
    }

    @XmlElement(name = "edition")
    public Integer getEdition() {
        return edition;
    }

    public void setEdition(Integer integer) {
        this.edition = integer;
    }

    @XmlElement(name = "totalVolumes")
    public Integer getTotal() {
        return totalCount;
    }

    public void setTotal(Integer integer) {
        this.totalCount = integer;
    }

    public String[] toStrings() {
        return new String[]{getName(), getAuthor().toString(), getPubl(), getNumb().toString(), getEdition().toString(),
                getTotal().toString()};
    }

    @Override
    public boolean equals(Object object) {
        Book book = (Book) object;
        if (this.author.equals(book.getAuthor()) && (this.edition.equals(book.getEdition())) &&
                this.name.equalsIgnoreCase(book.getName()) && this.publisher.equalsIgnoreCase(book.getPubl()) &&
                (this.tomesNumber.equals(book.getNumb())) && (this.totalCount.equals(book.getTotal()))) {
            return true;
        } else {
            return false;
        }
    }
}
