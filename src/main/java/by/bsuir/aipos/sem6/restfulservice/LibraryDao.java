package by.bsuir.aipos.sem6.restfulservice;

import org.glassfish.hk2.utilities.reflection.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.StringReader;
import java.util.List;

/**
 * Created by Dzmitry Saladukha on 18.04.2016.
 */
public class LibraryDao {
    private Library library;

    public LibraryDao() {
        File file = new File("Data1.xml");
        try {
            JAXBContext context = JAXBContext.newInstance(Library.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            library = (Library) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            System.err.println("JAXB-exception");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Library getLibrary() {
        return library;
    }

    public List<Book> searchByAuthor(String xmlAuthor) {
        Author author = new Author();
        try {
            author = unmarshalAuthor(xmlAuthor);
        } catch (JAXBException e) {
            Logger.getLogger().warning("Incorrect query parameters");
        }
        return library.searchByAuthor(author);
    }

    public Integer eraseByAuthor(String xmlAuthor) {
        Author author = new Author();
        try {
            author = unmarshalAuthor(xmlAuthor);
        } catch (JAXBException e) {
            Logger.getLogger().warning("Incorrect query parameters");
        }
        Integer res = library.erase(library.searchByAuthor(author));
        saveState();
        return res;
    }

    public Integer eraseByAuthor(Author author) {
        Integer res = library.erase(library.searchByAuthor(author));
        saveState();
        return res;
    }

    public Boolean erase(String xmlBook) {
        Book book = new Book();
        try {
            book = unmarshalBook(xmlBook);
        } catch (JAXBException e) {
            Logger.getLogger().warning("Incorrect query parameters");
        }
        Boolean rez = library.erase(book);
        saveState();
        return rez;
    }

    private void saveState() {
        File file = new File("Data1.xml");
        try {
            JAXBContext context = JAXBContext.newInstance(Library.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.marshal(library, file);
        } catch (JAXBException e) {
            System.err.println("JAXB-exception");
            e.printStackTrace();
        }
    }

    private Author unmarshalAuthor(String xmlAuthor) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Author.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        StringReader reader = new StringReader(xmlAuthor);
        return (Author) unmarshaller.unmarshal(reader);
    }

    private Book unmarshalBook(String xmlBook) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Book.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        StringReader reader = new StringReader(xmlBook);
        return (Book) unmarshaller.unmarshal(reader);
    }

    public Boolean addBook(String xmlBook) {
        Book book = new Book();
        try {
            book = unmarshalBook(xmlBook);
        } catch (JAXBException e) {
            Logger.getLogger().warning("Incorrect query parameters");
        }
        Boolean rez = library.addBook(book);
        saveState();
        return rez;
    }
}
