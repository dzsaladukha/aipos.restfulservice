package by.bsuir.aipos.sem6.restfulservice;

import org.glassfish.jersey.process.internal.RequestScoped;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Dzmitry Saladukha on 18.04.2016.
 */
@RequestScoped
@Path("/LibraryService")
public class LibraryService {

    private LibraryDao libDao = new LibraryDao();

    @GET
    @Path("/library")
    @Produces(MediaType.APPLICATION_XML)
    public Library getLibrary() {
        System.out.println("Sending entire library");
        return libDao.getLibrary();
    }

    @POST
    @Path("/search/author")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.APPLICATION_XML)
    public List<Book> searchByAuthor(String xmlAuthor) {
        System.out.println("Searching by author:\n" + xmlAuthor);
        return libDao.searchByAuthor(xmlAuthor);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_XML)
    public Boolean erase(String xmlBook) {
        System.out.println("Erasing:\n" + xmlBook);
        return libDao.erase(xmlBook);
    }

    @DELETE
    @Path("/author/{surname}/{firstname}")
    public String eraseByAuthor(@PathParam("surname") String surname, @PathParam("firstname") String firstname) {
        Author author = new Author();
        author.setSurname(surname);
        author.setFirstName(firstname);
        System.out.println("Erasing by author:\n" + author.toString());
        return libDao.eraseByAuthor(author).toString();
    }

    @DELETE
    @Path("/author")
    @Consumes(MediaType.APPLICATION_XML)
    public Integer eraseByAuthor(String xmlAuthor) {
        System.out.println("Erasing by author:\n" + xmlAuthor);
        return libDao.eraseByAuthor(xmlAuthor);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public Response addBook(String xmlBook) {
        System.out.println("Adding book:\n" + xmlBook);
        if (libDao.addBook(xmlBook)) {
            return Response.status(201).build();
        }
        return Response.status(202).build();
    }
}
