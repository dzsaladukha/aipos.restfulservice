package by.bsuir.aipos.sem6.restfulservice;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dzmitry Saladukha on 18.04.2016.
 */
@XmlRootElement(name = "library")
public class Library {

    private List<Book> library = new ArrayList<Book>();

    @XmlElements({@XmlElement(name = "book", type = Book.class)})
    public List<Book> getLibrary() {
        return library;
    }

    public void setLibrary(List<Book> library) {
        this.library = library;
    }

    public List<Book> searchByAuthor(Author author) {
        List<Book> list = new ArrayList<Book>();

        for (Book curBook : library) {
            if (curBook.getAuthor().equals(author)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByName(String name) {
        List<Book> list = new ArrayList<Book>();

        for (Book curBook : library) {
            if (curBook.getName().equalsIgnoreCase(name)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByAuthorAndPublisher(Author author, String publisher) {
        List<Book> list = new ArrayList<Book>();

        for (Book curBook : library) {
            if (curBook.getAuthor().equals(author) && curBook.getPubl().equalsIgnoreCase(publisher)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByAuthorAndTomesNumber(Author author, int a, int b) {
        List<Book> list = new ArrayList<Book>();

        for (Book curBook : library) {
            if (curBook.getAuthor().equals(author) && (curBook.getNumb() >= a) && (curBook.getNumb() <= b)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByEdition(int a, int b) {
        List<Book> list = new ArrayList<Book>();

        for (Book curBook : library) {
            if ((curBook.getEdition() >= a) && (curBook.getEdition() <= b)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByTotalVolumes(int a, int b) {
        List<Book> list = new ArrayList<Book>();

        for (Book curBook : library) {
            if ((curBook.getTotal() >= a) && (curBook.getTotal() <= b)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public Integer erase(List<Book> list) {
        Integer count = 0;
        for (Book delBook : list) {
            if (library.remove(delBook)) {
                count++;
            }
        }
        return count;
    }

    public Boolean erase(Book book) {
        return library.remove(book);
    }

    public Boolean addBook(Book book) {
        for (Book curBook : library) {
            if (curBook.equals(book)) {
                return false;
            }
        }
        return library.add(book);
    }
}
