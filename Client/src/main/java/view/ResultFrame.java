package view;

import model.Book;

import javax.swing.JFrame;
import java.awt.Dimension;
import java.util.List;

@SuppressWarnings("serial")
public class ResultFrame extends JFrame {
    
    public ResultFrame(List<Book> library){
        super("Result");
        
        add(new PaginalComponent<Book>(library));
        
        setPreferredSize(new Dimension(760, 350));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
