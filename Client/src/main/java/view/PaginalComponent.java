package view;

import model.Book;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("serial")
public class PaginalComponent<T extends Book> extends JPanel {

    private JTable table = new JTable();
    private JPanel controlPanel = new JPanel();
    private List<T> library;
    private String[] headers;
    public JLabel totalElem;
    public JLabel pageNum;
    public JTextField numOnPage;
    private final PaginalComponentController<T> controller;

    public PaginalComponent(List<T> library) {

        this.library = library;
        this.headers = new String[] { "название книги", "автор",
                "издательство", "число томов", "тираж", "итого томов" };
        controller = new PaginalComponentController<T>(this);

        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(650, 250));

        add(loadContent(), BorderLayout.CENTER);
        add(createControlPanel(), BorderLayout.SOUTH);
    }

    public PaginalComponent(List<T> library, String[] headers) {

        this.library = library;
        this.headers = headers;
        controller = new PaginalComponentController<T>(this);

        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(650, 250));

        add(loadContent(), BorderLayout.CENTER);
        add(createControlPanel(), BorderLayout.SOUTH);
    }

    private JPanel createControlPanel() {
        controlPanel.setLayout(new FlowLayout());
        JButton first = new JButton("<<");
        JButton prev = new JButton("<");
        JButton next = new JButton(">");
        JButton last = new JButton(">>");
        String text = new String((new Integer(library.size()).toString()));
        numOnPage = new JTextField(text, 2);
        totalElem = new JLabel("из " + text);
        pageNum = new JLabel("Страница 1");

        first.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.toFirstPage();
            }

        });
        prev.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                controller.toPrevPage();
            }

        });
        next.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                controller.toNextPage();
            }

        });
        last.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                controller.toLastPage();
            }

        });
        numOnPage.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                controller.changeCountOfVisibleElem();
            }

        });

        controller.changeCountOfVisibleElem();

        controlPanel.add(first);
        controlPanel.add(prev);
        controlPanel.add(pageNum);
        controlPanel.add(next);
        controlPanel.add(last);
        controlPanel.add(numOnPage);
        controlPanel.add(totalElem);
        return controlPanel;
    }

    private JScrollPane loadContent() {
        DefaultTableModel items = new DefaultTableModel();
        this.table.setModel(items);
        JScrollPane scroll = new JScrollPane(table);

        items.setColumnIdentifiers(headers);

        for (int index = 0; index < library.size(); index++) {
            items.addRow(library.get(index).toStrings());
        }
        this.table.setEnabled(false);

        return scroll;
    }

    public void updateTable() {
        DefaultTableModel items = new DefaultTableModel();
        items.setColumnIdentifiers(headers);
        for (int index = 0; index < library.size(); index++) {
            items.addRow(library.get(index).toStrings());
        }
        this.table.setModel(items);
        this.table.updateUI();

        String text = new String((new Integer(library.size()).toString()));
        numOnPage.setText(text);
        totalElem.setText("из " + text);
        pageNum.setText("Страница 1");
    }

    private void updateTable(List<T> libr) {
        DefaultTableModel items = new DefaultTableModel();
        items.setColumnIdentifiers(headers);
        for (int index = 0; index < libr.size(); index++) {
            items.addRow(libr.get(index).toStrings());
        }
        this.table.setModel(items);
        this.table.updateUI();

        numOnPage.setText(new String((new Integer(libr.size()).toString())));
        totalElem.setText("из "
                + new String((new Integer(library.size()).toString())));
    }

    public List<T> getLibrary() {
        return library;
    }

    public void setLibrary(List<T> library) {
        this.library = library;
        controller.setLibrary(library);
    }

    @SuppressWarnings("hiding")
    private class PaginalComponentController<T extends Book> {

        private List<T> pageElems = new LinkedList<T>();
        private List<T> library;
        private PaginalComponent<T> pagComp;
        private Integer countElems, curIndex = 0, curPage = 1;

        public PaginalComponentController(PaginalComponent<T> paginalComponent) {
            this.pagComp = paginalComponent;
            this.library = pagComp.getLibrary();
        }

        public void toFirstPage() {

            curPage = 1;
            curIndex = 0;
            combainPageElems(countElems);
            pagComp.updateTable(pageElems);
            pagComp.pageNum.setText("Страница 1");
        }

        public void toPrevPage() {

            curPage--;
            if (curPage < 2) {
                toFirstPage();
                return;
            }
            curIndex = (curPage - 1) * countElems;

            combainPageElems(countElems);
            pagComp.updateTable(pageElems);
            pagComp.pageNum.setText("Страница " + curPage);
        }

        public void toNextPage() {

            curPage++;
            if (curPage > library.size() / countElems) {
                toLastPage();
                return;
            }
            curIndex = (curPage - 1) * countElems;

            combainPageElems(countElems);
            pagComp.updateTable(pageElems);
            pagComp.pageNum.setText("Страница " + curPage);
        }

        public void toLastPage() {

            if (library.size() % countElems == 0) {
                curPage = library.size() / countElems;
            } else {
                curPage = library.size() / countElems + 1;
            }
            curIndex = (curPage - 1) * countElems;

            combainPageElems(library.size() - curIndex);
            pagComp.updateTable(pageElems);
            pagComp.pageNum.setText("Страница " + curPage);
        }

        public void combainPageElems(int length) {

            pageElems.clear();
            for (int index = curIndex; index < (curIndex + length); index++) {
                pageElems.add(library.get(index));
            }
        }

        public void changeCountOfVisibleElem() {

            if (library.size() >= Integer.parseInt(pagComp.numOnPage.getText())) {
                countElems = Integer.parseInt(pagComp.numOnPage.getText());

                toFirstPage();
            }
        }

        public void setLibrary(List<T> library) {
            this.library = library;
        }
    }
}
