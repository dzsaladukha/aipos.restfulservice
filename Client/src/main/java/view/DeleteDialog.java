package view;

import controller.Service;
import model.Book;
import model.Library;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.ws.rs.core.MediaType;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class DeleteDialog extends JDialog {
    private DeleteByAuthor del;
    private JPanel inputPanel;
    private JButton button;
    private PaginalComponent<Book> paginComp;

    public DeleteDialog(PaginalComponent<Book> paginComp) {
        this.paginComp = paginComp;

        setTitle("Delete");
        setLayout(new BorderLayout());
        add(createInputPanel(), BorderLayout.CENTER);
        add(createButton(), BorderLayout.SOUTH);

        setPreferredSize(new Dimension(300, 240));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private Component createButton() {
        button = new JButton("Delete");
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String quantity = del.delete();
                if (!quantity.isEmpty()) {
                    JOptionPane.showMessageDialog(null, quantity
                            + " элементов удалено.");
                    paginComp.setLibrary(Service.INSTANCE.webTarget
                            .path("/library")
                            .request(MediaType.APPLICATION_XML)
                            .get(Library.class).getLibrary());
                    paginComp.updateTable();
                } else {
                    JOptionPane.showMessageDialog(null, "Nothing was found.");
                }
                dispose();
            }
        });
        return button;
    }

    private Component createInputPanel() {
        inputPanel = new JPanel();
        del = new DeleteByAuthor();
        inputPanel.add((Component) del);
        return inputPanel;
    }

    private class DeleteByAuthor extends JPanel {

        private JTextField firstName;
        private JTextField surname;
        @SuppressWarnings("unused")
        private JTextField patronymic;

        public DeleteByAuthor() {

            setLayout(new GridLayout(3, 2));
            setPreferredSize(new Dimension(250, 80));

            add(new JLabel("Фамилия автора"));
            add(surname = new JTextField());
            add(new JLabel("Имя автора"));
            add(firstName = new JTextField());
            add(new JLabel("Отчество автора"));
            add(patronymic = new JTextField());
        }

        public String delete() {

            String result = Service.INSTANCE.webTarget
                    .path("/author/{surname}/{firstname}")
                    .resolveTemplate("surname", surname.getText())
                    .resolveTemplate("firstname", firstName.getText())
                    .request(MediaType.APPLICATION_XML).delete(String.class);

            return result;
        }
    }
}
