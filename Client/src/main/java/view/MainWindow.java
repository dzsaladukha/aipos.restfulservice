package view;

import controller.SaveFileListener;
import controller.Service;
import model.Book;
import model.Library;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.ws.rs.core.MediaType;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainWindow {
    private JFrame mainFrame;
    private Library model;
    private PaginalComponent<Book> paginComp;

    public MainWindow(Library mdl) {

        setModel(mdl);
        paginComp = new PaginalComponent<Book>(mdl.getLibrary());

        mainFrame = new JFrame("Some library");
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        mainFrame.add(createToolBar(), BorderLayout.WEST);
        mainFrame.add(paginComp, BorderLayout.CENTER);

        mainFrame.setPreferredSize(new Dimension(960, 450));
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    private JToolBar createToolBar() {
        JToolBar toolBar = new JToolBar();
        JButton bNew = new JButton("New");
        JButton bUpdate = new JButton("Update");
        JButton bSave = new JButton("Save");
        JButton bSearch = new JButton("Search");
        JButton bDelete = new JButton("Delete");

        bNew.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                new NewDialog(paginComp);
            }
        });
        bUpdate.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                paginComp.setLibrary(Service.INSTANCE.webTarget
                        .path("/library").request(MediaType.APPLICATION_XML)
                        .get(Library.class).getLibrary());
                paginComp.updateTable();
            }
        });
        bSave.addActionListener(new SaveFileListener(new Library(paginComp.getLibrary())));
        bSearch.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new SearchDialog(paginComp);
            }
        });
        bDelete.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new DeleteDialog(paginComp);
            }
        });

        toolBar.add(bNew);
        toolBar.add(bUpdate);
        toolBar.add(bSave);
        toolBar.add(bSearch);
        toolBar.add(bDelete);

        toolBar.setOrientation(SwingConstants.VERTICAL);

        return toolBar;
    }

    public Library getModel() {
        return model;
    }

    public void setModel(Library model) {
        this.model = model;
    }

    public PaginalComponent<Book> getPaginComp() {
        return paginComp;
    }
}
