package view;

import controller.NewDialogListener;
import model.Author;
import model.Book;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

@SuppressWarnings("serial")
public class NewDialog extends JDialog {

    private JTextField tf1;
    private JTextField tf2;
    private JTextField tf3;
    private JTextField tf4;
    private JTextField tf5;
    private JTextField tf6;
    private JTextField tf7;
    private PaginalComponent<Book> pagComp;

    public NewDialog(PaginalComponent<Book> pagComp) {

        setPaginComp(pagComp);

        setTitle("Create New Element");
        setLayout(new BorderLayout());
        add(centralPane(), BorderLayout.CENTER);
        add(controlPane(), BorderLayout.SOUTH);

        setPreferredSize(new Dimension(350, 250));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private JPanel controlPane() {

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        JButton but = new JButton("Create");

        but.addActionListener(new NewDialogListener(this));

        panel.add(but);

        return panel;
    }

    private Component centralPane() {

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(7, 2));

        JLabel lbl1 = new JLabel("название книги");
        JLabel lbl2 = new JLabel("Фамилия автора");
        JLabel lbl3 = new JLabel("Имя автора");
        JLabel lbl4 = new JLabel("издательство");
        JLabel lbl5 = new JLabel("число томов");
        JLabel lbl6 = new JLabel("тираж");
        JLabel lbl7 = new JLabel("итого томов");
        tf1 = new JTextField();
        tf2 = new JTextField();
        tf3 = new JTextField();
        tf4 = new JTextField();
        tf5 = new JTextField();
        tf6 = new JTextField();
        tf7 = new JTextField();

        panel.add(lbl1);
        panel.add(tf1);
        panel.add(lbl2);
        panel.add(tf2);
        panel.add(lbl3);
        panel.add(tf3);
        panel.add(lbl4);
        panel.add(tf4);
        panel.add(lbl5);
        panel.add(tf5);
        panel.add(lbl6);
        panel.add(tf6);
        panel.add(lbl7);
        panel.add(tf7);

        return panel;
    }

    public Book createNewElem() {

        Book newElem = new Book();
        Author author = new Author();
        try {
        newElem.setName(tf1.getText());
        author.setSurname(tf2.getText());
        author.setFirstName(tf3.getText());
        newElem.setAuthor(author);
        newElem.setPubl((tf4.getText()));
        newElem.setNumb(Integer.parseInt((tf5.getText())));
        newElem.setEdition(Integer.parseInt((tf6.getText())));
        newElem.setTotal(Integer.parseInt((tf7.getText())));
        } catch (NumberFormatException ex){
            JOptionPane.showMessageDialog(null, "Incorrect input!");
            return null;
        }
        System.out.println("New book created");
        return newElem;
    }

    public PaginalComponent<Book> getPaginComp() {
        return pagComp;
    }

    public void setPaginComp(PaginalComponent<Book> pagComp) {
        this.pagComp = pagComp;
    }
}
