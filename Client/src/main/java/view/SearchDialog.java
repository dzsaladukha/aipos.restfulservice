package view;

import controller.Service;
import model.Author;
import model.Book;
import model.Library;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@SuppressWarnings("serial")
public class SearchDialog extends JDialog {

    protected JButton button;
    protected SearchByAuthor str;
    private JPanel inputPanel;
    private PaginalComponent<Book> paginComp;

    public SearchDialog(PaginalComponent<Book> paginComp) {
        this.paginComp = paginComp;

        setTitle("Search");
        setLayout(new BorderLayout());
        add(createInputPanel(), BorderLayout.CENTER);
        add(createButton(), BorderLayout.SOUTH);

        setPreferredSize(new Dimension(300, 240));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private Component createButton() {
        button = new JButton("Search");
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> list = str.search();
                if (!list.isEmpty()) {
                    new ResultFrame(list);
                    paginComp.setLibrary(Service.INSTANCE.webTarget
                            .path("/library")
                            .request(MediaType.APPLICATION_XML)
                            .get(Library.class).getLibrary());
                    paginComp.updateTable();
                } else {
                    JOptionPane.showMessageDialog(null, "Nothing was found.");
                }
                dispose();
            }
        });
        return button;
    }

    private Component createInputPanel() {
        inputPanel = new JPanel();
        str = new SearchByAuthor();
        inputPanel.add((Component) str);
        return inputPanel;
    }

    private class SearchByAuthor extends JPanel {

        private JTextField firstName;
        private JTextField surname;
        private JTextField patronymic;

        public SearchByAuthor() {

            setLayout(new GridLayout(3, 2));
            setPreferredSize(new Dimension(250, 80));

            add(new JLabel("Фамилия автора"));
            add(surname = new JTextField());
            add(new JLabel("Имя автора"));
            add(firstName = new JTextField());
            add(new JLabel("Отчество автора"));
            add(patronymic = new JTextField());
        }

        public List<Book> search() {
            Author author = new Author();
            author.setSurname(surname.getText());
            author.setFirstName(firstName.getText());
            author.setPatronymic(patronymic.getText());

            GenericType<List<Book>> list = new GenericType<List<Book>>() {
            };
            return Service.INSTANCE.webTarget
                    .path("/search/author")
                    .request(MediaType.APPLICATION_XML)
                    .post(Entity.entity(author, MediaType.APPLICATION_XML),
                            list);
        }
    }
}
