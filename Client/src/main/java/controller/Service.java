package controller;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public enum Service {
    INSTANCE;

    private final Client client;
    private final String endPoint = "http://localhost:8080/LibraryService/";
    public WebTarget webTarget;

    private Service() {
        client = ClientBuilder.newClient();
        webTarget = client.target(endPoint);
    }
}
