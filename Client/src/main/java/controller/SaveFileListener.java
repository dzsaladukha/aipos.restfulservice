package controller;

import model.Library;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class SaveFileListener implements ActionListener{
    
    private Library model;
    
    public SaveFileListener(Library mdl) {

        this.model = mdl;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {

        JFileChooser fileopen = new JFileChooser();
        fileopen.setFileFilter(new FileNameExtensionFilter("XML Files", "xml"));
        fileopen.setCurrentDirectory(new File(this.getClass().getResource("/").getPath() + "/../"));
        int ret = fileopen.showSaveDialog(null);                
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileopen.getSelectedFile();
            if(!file.getPath().toLowerCase().endsWith(".xml")) {
                file = new File(file.toPath()+".xml");
                }
        try {
            JAXBContext context = JAXBContext.newInstance(Library.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(model, file);
        } catch (JAXBException e) { 
            System.out.println("JAXB-exception");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        }
    }
}
