package controller;

import model.Book;
import model.Library;
import view.NewDialog;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewDialogListener implements ActionListener {

    private NewDialog newDialog;

    public NewDialogListener(NewDialog newDialog) {

        this.newDialog = newDialog;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {

        Book book = newDialog.createNewElem();
        if (book != null) {
            Service.INSTANCE.webTarget.request(MediaType.APPLICATION_XML).put(
                    Entity.entity(book, MediaType.APPLICATION_XML), Book.class);
            newDialog.getPaginComp().setLibrary(
                    Service.INSTANCE.webTarget.path("/library")
                            .request(MediaType.APPLICATION_XML)
                            .get(Library.class).getLibrary());
            newDialog.getPaginComp().updateTable();
        }
        newDialog.dispose();
    }
}
