package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "author")
public class Author implements Serializable {
    
    private static final long serialVersionUID = 6820005223004001810L;
    private String surname;
    private String firstName;
    private String patronymic;

    @XmlElement(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @XmlElement(name = "patronymic")
    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    @XmlElement(name = "firstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String toString() {
        return firstName + " " + surname;
    }

    @Override
    public boolean equals(Object object) {
        Author author = (Author) object;
        if (this.getFirstName().equalsIgnoreCase(author.getFirstName())
                && this.getSurname().equalsIgnoreCase(author.getSurname())) {
            return true;
        } else {
            return false;
        }
    }
}
