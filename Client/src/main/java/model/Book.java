package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "book")
public class Book implements Serializable {

    private static final long serialVersionUID = 5866651575341222537L;
    private String name;
    private Author author;
    private String publisher;
    private Integer edition;
    private Integer tomesNumber;
    private Integer totalCount;

    public void setName(String string) {
        this.name = string;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setPubl(String string) {
        this.publisher = string;
    }

    public void setNumb(Integer integer) {
        this.tomesNumber = integer;
    }

    public void setEdition(Integer integer) {
        this.edition = integer;
    }

    public void setTotal(Integer integer) {
        this.totalCount = integer;
    }

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    @XmlElement(type = Author.class)
    public Author getAuthor() {
        return author;
    }

    @XmlElement(name = "publisher")
    public String getPubl() {
        return publisher;
    }

    @XmlElement(name = "volumeNumber")
    public Integer getNumb() {
        return tomesNumber;
    }

    @XmlElement(name = "edition")
    public Integer getEdition() {
        return edition;
    }

    @XmlElement(name = "totalVolumes")
    public Integer getTotal() {
        return totalCount;
    }

    public String[] toStrings() {
        return new String[] { getName(), getAuthor().toString(), getPubl(),
                getNumb().toString(), getEdition().toString(),
                getTotal().toString() };
    }

    @Override
    public boolean equals(Object object) {
        Book book = (Book) object;
        if (this.author.equals(book.getAuthor())
                && (this.edition.equals(book.getEdition()))
                && this.name.equalsIgnoreCase(book.getName())
                && this.publisher.equalsIgnoreCase(book.getPubl())
                && (this.tomesNumber.equals(book.getNumb()))
                && (this.totalCount.equals(book.getTotal()))) {
            return true;
        } else {
            return false;
        }
    }
}
