package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "library")
public class Library implements Serializable {

    private static final long serialVersionUID = 4888322643790519515L;
    private List<Book> library = new LinkedList<Book>();

    @XmlElements({ @XmlElement(name = "book", type = Book.class) })
    public List<Book> getLibrary() {
        return library;
    }

    public Library() {
    }

    public Library(List<Book> library) {
        this.library = library;
    }

    public void setLibrary(List<Book> library) {
        this.library = library;
    }

    public List<Book> searchByAuthor(Author author) {
        List<Book> list = new LinkedList<Book>();

        for (Book curBook : library) {
            if (curBook.getAuthor().equals(author)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByName(String name) {
        List<Book> list = new LinkedList<Book>();

        for (Book curBook : library) {
            if (curBook.getName().equalsIgnoreCase(name)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByAuthorAndPublisher(Author author, String publisher) {
        List<Book> list = new LinkedList<Book>();

        for (Book curBook : library) {
            if (curBook.getAuthor().equals(author)
                    && curBook.getPubl().equalsIgnoreCase(publisher)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByAuthorAndTomesNumber(Author author, int a, int b) {
        List<Book> list = new LinkedList<Book>();

        for (Book curBook : library) {
            if (curBook.getAuthor().equals(author) && (curBook.getNumb() >= a)
                    && (curBook.getNumb() <= b)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByEdition(int a, int b) {
        List<Book> list = new LinkedList<Book>();

        for (Book curBook : library) {
            if ((curBook.getEdition() >= a) && (curBook.getEdition() <= b)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public List<Book> searchByTotalVolumes(int a, int b) {
        List<Book> list = new LinkedList<Book>();

        for (Book curBook : library) {
            if ((curBook.getTotal() >= a) && (curBook.getTotal() <= b)) {
                list.add(curBook);
            }
        }

        return list;
    }

    public void erase(List<Book> list) {
        for (Book delBook : list) {
            library.remove(delBook);
        }
    }
}
