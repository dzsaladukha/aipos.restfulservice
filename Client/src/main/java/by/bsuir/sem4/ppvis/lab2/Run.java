package by.bsuir.sem4.ppvis.lab2;

import controller.Service;
import model.Library;
import view.MainWindow;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.ws.rs.core.MediaType;

public class Run {

    public static void main(String args[]) {
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            }
        }

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Library library = Service.INSTANCE.webTarget.path("/library").request(MediaType.APPLICATION_XML)
                        .get(Library.class);

                new MainWindow(library);
            }
        });
    }
}
